package com.epam;
import java.util.Scanner;
/**
 * Class application.
 * Kurudz Mykhailo
 * @version 1.0
 */
public class Application {
    /** Start program
     * program about odd and even numbers
     */
    public static void main(String[] args) {
        Scanner startVal = new Scanner(System.in);
        Scanner endVal = new Scanner(System.in);
        System.out.print("Enter start value: ");
        /** first value in array */
        int startValue = startVal.nextInt();
        System.out.print("Enter end value: ");
        /** end value in array */
        int endValue = endVal.nextInt();
        if (endValue < startValue) {
            System.out.print("end value greater than start value");
        } else {
            int size = endValue - startValue + 1;
            int[] arr = new int[size];

            for (int i = 0; i < size; i++) {
                arr[i] = startValue;
                startValue++;
            }
            int sumOdd = 0;
            int sumEven = 0;
            System.out.print("Odd elements: ");
            int j = 0;
            while (j < size) {
                if (arr[j] % 2 != 0) {
                    System.out.print(arr[j] + " ");
                    sumOdd += arr[j];
                    j += 2;
                } else {
                    j++;
                }
            }
            System.out.println();

            System.out.print("Even elements: ");
            int k = size - 1;
            while (k > 0) {
                if (arr[k] % 2 == 0) {
                    System.out.print(arr[k] + " ");
                    sumEven += arr[k];
                    k -= 2;
                } else {
                    k--;
                }
            }
            System.out.println();
            System.out.println("Sum odd = " + sumOdd);
            System.out.println("Sum even = " + sumEven);

            Scanner sizeFibonacci = new Scanner(System.in);
            int sizeFib = sizeFibonacci.nextInt();
            final int maxIteration = 45;
            if (sizeFib > maxIteration) {
                System.out.println("very big size");
            } else {

                int[] arrFib = new int[sizeFib];
                for (int l = 0; l < sizeFib; l++) {
                    if (l > 2) {
                        arrFib[l] = arrFib[l - 1] + arrFib[l - 2];
                    } else {
                        arrFib[l] = l;
                    }
                }
                int maxOdd = arrFib[0];
                int maxEven = arrFib[0];
                for (int i = 0; i < sizeFib; i++) {
                    if (arrFib[i] > maxOdd && arrFib[i] % 2 != 0) {
                        maxOdd = arrFib[i];
                    }
                    if (arrFib[i] > maxEven && arrFib[i] % 2 == 0) {
                        maxEven = arrFib[i];
                    }
                }
                System.out.println("Max odd = " + maxOdd);
                System.out.println("Max even = " + maxEven);
                int numOdd = 0;
                for (int i = 0; i < sizeFib; i++) {
                    if (arrFib[i] % 2 != 0) {
                        numOdd++;
                    }
                }
                final int percent = 100;
                float res = (numOdd * percent) / (sizeFib);
                System.out.println("percent odd  = " + res);
                System.out.println("percent even  = " + (percent - res));

            }
        }
    }

}

